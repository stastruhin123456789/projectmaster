from TestControlGPIO import TestControlGPIO

test_control_gpio = TestControlGPIO()


def json_to_text(data):
    text = ''
    for i in data:
        text = text + i + '\n'
        for j in data[i]:
            text = text + '{0}: {1}'.format(j, 'on' if (data[i][j] is True) else 'off') + '\n'
    return text


def check(cgpio):
    print('check 123')
    if cgpio.status['out']['1'] or cgpio.status['out']['2'] or cgpio.status['out']['3']:
        print('check not N1 or N2')
        if not (cgpio.status['out']['N1'] or cgpio.status['out']['N2']):
            print('check VX')
            if cgpio.status['out']['VX']:
                pass
            else:
                cgpio.set_status_out({'VX': True})
            print('check B')
            if cgpio.status['out']['B']:
                cgpio.set_status_out({'B': False})
            else:
                pass

            print('check R')
            cgpio.get_status_in_r()
            if cgpio.status['in']['R']:
                print('There is water flow from VX')
                # end of check
                return
            else:
                print('check BDV')
                cgpio.get_status_in_bdv()
                if cgpio.status['in']['BDV']:
                    print('Tank full with water')
                else:
                    cgpio.set_status_out({'B': True})
                # continuation (if BDN) below
        else:
            # continuation if BDN) below
            pass
        # continuation (if BDN)
        print('check BDN')
        cgpio.get_status_in_bdn()
        if cgpio.status['in']['BDN']:
            print('check N1')
            if cgpio.status['out']['N1']:
                print('check R')
                cgpio.get_status_in_r()
                if cgpio.status['in']['R']:
                    print('There is water flow from N1')
                    # end of check
                    return
                else:
                    print('N1 crash')
                    cgpio.set_status_crash({'N1': True})
                    cgpio.set_status_out({'N1': False})
                    # continuation (if N2) below
            else:
                pass
                # continuation (if N2) below
            # continuation (if N2)
            print('check N2')
            if cgpio.status['out']['N2']:
                print('check R')
                cgpio.get_status_in_r()
                if cgpio.status['in']['R']:
                    print('There is water flow from N2')
                    # end of check
                    return
                else:
                    print('N2 crash')
                    cgpio.set_status_crash({'N2': True})
                    cgpio.set_status_out({'N2': False})
                    # end of check
                    return
            else:
                cgpio.set_status_out({'N1': True})
                print('check R')
                cgpio.get_status_in_r()
                if cgpio.status['in']['R']:
                    print('There is water flow from N1')
                    # end of check
                    return
                else:
                    print('N1 crash')
                    cgpio.set_status_crash({'N1': True})
                    cgpio.set_status_out({'N1': False})
                    cgpio.set_status_out({'N2': True})
                    print('check R')
                    cgpio.get_status_in_r()
                    if cgpio.status['in']['R']:
                        print('There is water flow from N2')
                        # end of check
                        return
                    else:
                        print('N2 crash')
                        cgpio.set_status_crash({'N2': True})
                        cgpio.set_status_out({'N2': False})
                        # end of check
                        return
        else:
            print('check N1')
            if cgpio.status['out']['N1']:
                cgpio.set_status_out({'N1': False})
            else:
                pass
            print('check N2')
            if cgpio.status['out']['N2']:
                cgpio.set_status_out({'N2': False})
            else:
                pass
            print('Tank empty without water')
    else:
        print('check BDV')
        cgpio.get_status_in_bdv()
        if cgpio.status['in']['BDV']:
            print('check B')
            if cgpio.status['out']['B']:
                cgpio.set_status_out({'B': False})
            else:
                pass
            print('check VX')
            if cgpio.status['out']['VX']:
                cgpio.set_status_out({'VX': False})
            else:
                pass
            print('Tank full with water')
        else:
            print('check B')
            if cgpio.status['out']['B']:
                pass
            else:
                cgpio.set_status_out({'B': True})
            print('check VX')
            if cgpio.status['out']['VX']:
                pass
            else:
                cgpio.set_status_out({'VX': True})
    print('End check')


st_out = {
    '3': True,
    '2': False,
    '1': False,
    'VX': True,
    'B': True,
    'N1': False,
    'N2': False
}

test_control_gpio.set_status_out(st_out)
check(test_control_gpio)
print(test_control_gpio.get_status())
print(json_to_text(test_control_gpio.get_status()))
