import requests
import json
import time
from BotHandler import BotHandler
from RemoteMainServer import RemoteMainServer


def json_to_text(data):
    text = ''
    for i in data:
        text = text + i + '\n'
        for j in data[i]:
            text = text + '{0}: {1}'.format(j, 'on' if (data[i][j] is True) else 'off') + '\n'
    return text


def connect_to_internet():
    while True:
        try:
            resp = requests.get("https://api.telegram.org/")
            if resp.status_code == 200:
                break
        except requests.exceptions.RequestException as e:
            print('..')
            time.sleep(1)
            continue


remote_main_server = RemoteMainServer()
token = ''
bot = BotHandler(token)


def main():
    connect_to_internet()

    new_offset = None
    while True:
        bot.get_updates(new_offset)

        last_update = bot.get_last_update()
        if not last_update:
            continue

        last_update_id = last_update['update_id']
        last_chat_text = last_update['message']['text']
        last_chat_id = last_update['message']['chat']['id']
        last_chat_name = last_update['message']['chat']['first_name']

        if last_chat_text.lower() == '/start':
            bot.send_message(last_chat_id, 'Hello {0}'.format(last_chat_name))
        elif last_chat_text.lower() in ['status', '/status']:
            status = remote_main_server.get_status()
            bot.send_message(last_chat_id, json_to_text(status))

        elif last_chat_text.lower() in ['1 on', '/1on']:
            remote_main_server.set_status_out_123_now({'1': True})
            bot.send_message(last_chat_id, '1 on')
        elif last_chat_text.lower() in ['1 off', '/1off']:
            remote_main_server.set_status_out_123_now({'1': False})
            bot.send_message(last_chat_id, '1 off')

        elif last_chat_text.lower() in ['2 on', '/2on']:
            remote_main_server.set_status_out_123_now({'2': True})
            bot.send_message(last_chat_id, '2 on')
        elif last_chat_text.lower() in ['2 off', '/2off']:
            remote_main_server.set_status_out_123_now({'2': False})
            bot.send_message(last_chat_id, '2 off')

        elif last_chat_text.lower() in ['3 on', '/3on']:
            remote_main_server.set_status_out_123_now({'3': True})
            bot.send_message(last_chat_id, '3 on')
        elif last_chat_text.lower() in ['3 off', '/3off']:
            remote_main_server.set_status_out_123_now({'3': False})
            bot.send_message(last_chat_id, '3 off')

        else:
            bot.send_message(last_chat_id, 'not command')

        bot.send_message_with_reply_markup(
            last_chat_id, 'Select from the list in the keyboard',
            json.dumps({
                'keyboard': [
                    [{'text': 'status'}],
                    [{'text': '1 on'}, {'text': '1 off'}],
                    [{'text': '2 on'}, {'text': '2 off'}],
                    [{'text': '3 on'}, {'text': '3 off'}]
                ],
                'one_time_keyboard': False,
                'resize_keyboard': True})
        )

        new_offset = last_update_id + 1


if __name__ == '__main__':
    remote_main_server.connect_to_main_server()
    print('Run')
    try:
        main()
    except KeyboardInterrupt:
        exit()
