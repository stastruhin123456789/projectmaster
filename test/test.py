import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

N1 = 26
GPIO.setup(N1, GPIO.OUT, initial=GPIO.LOW)

while True:
    print('1-True, 0-False')
    if input() == '1':
        GPIO.output(N1, GPIO.HIGH)
    else:
        GPIO.output(N1, GPIO.LOW)
