class TestControlGPIO:
    def __init__(self):
        self.status = {
            'in': {
                'R': False,
                'BDV': False,
                'BDN': False,
            },
            'out': {
                '3': False,
                '2': False,
                '1': False,
                'VX': True,
                'B': True,
                'N1': False,
                'N2': False
            },
            'crash': {
                'N1': False,
                'N2': False
            }
        }

    def get_status(self):
        return self.status

    def set_status_in(self, def_status_in):
        self.status['in'].update(def_status_in)

    def set_status_out(self, def_status_out):
        status_out = self.status['out']
        for i in def_status_out:
            if def_status_out[i] != status_out[i]:
                pass
        self.status['out'].update(def_status_out)

    def set_status_crash(self, def_status_crash):
        self.status['crash'].update(def_status_crash)

    def get_status_in_bdv(self):
        print('Enter  BDV: 1 - True, 0 - False')
        if input() == '1':
            self.set_status_in({'BDV': True})
        else:
            self.set_status_in({'BDV': False})
        return self.status['in']['BDV']

    def get_status_in_bdn(self):
        print('Enter  BDN: 1 - True, 0 - False')
        if input() == '1':
            self.set_status_in({'BDN': True})
        else:
            self.set_status_in({'BDN': False})
        return self.status['in']['BDN']

    def get_status_in_r(self):
        print('Enter  R: 1 - True, 0 - False')
        if input() == '1':
            self.set_status_in({'R': True})
        else:
            self.set_status_in({'R': False})
        return self.status['in']['R']

    def set_status_default(self):
        self.set_status_in({'R': False})
        self.set_status_crash({'N1': False, 'N2': False})