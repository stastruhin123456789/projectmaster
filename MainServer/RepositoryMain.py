class RepositoryMain:
    def __init__(self):
        self.status_out_123_now = {
            '3': False,
            '2': False,
            '1': False
        }
        self.status = {
            'in': {
                'R': False,
                'BDV': False,
                'BDN': False,
            },
            'out': {
                '3': False,
                '2': False,
                '1': False,
                'VX': True,
                'B': True,
                'N1': False,
                'N2': False
            },
            'crash': {
                'N1': False,
                'N2': False
            }
        }

    def get_status(self):
        return self.status

    def set_status(self, s):
        self.status.update(s)

    def get_status_out_123_now(self):
        return self.status_out_123_now

    def set_status_out_123_now(self, s_out_123_now):
        self.status_out_123_now.update(s_out_123_now)
