from flask import Flask, render_template, redirect
from RemoteMainServer import RemoteMainServer
import subprocess

app = Flask(__name__)
remove_main_server = RemoteMainServer()
remove_main_server.connect_to_main_server()


@app.route('/')
def home():
    data = remove_main_server.get_status()
    return render_template('home.html', data=data)


@app.route('/<pin>/<state>')
def set_status_out(pin, state):
    data = {}
    if pin in ['1', '2', '3']:
        if state in ['on', 'off']:
            data[pin] = (state == 'on')
            remove_main_server.set_status_out_123_now(data)
    return redirect('/')


def get_ip_wifi():
    bash_command = "sudo hostname -I"
    process = subprocess.Popen(bash_command.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    ip = str(output).split('\'')[1].split()[0]
    return ip


if __name__ == '__main__':
    app.run(host=get_ip_wifi(), port=80)
    # app.run(host='192.168.0.108', port=80)
    # app.run(host='172.20.10.4', port=80)
