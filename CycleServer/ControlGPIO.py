import time

import RPi.GPIO as GPIO


class ControlGPIO():
    def __init__(self):
        self.pins = {
            'R': 21,
            'BDV': 20,
            'BDN': 16,
            '3': 26,
            '2': 19,
            '1': 13,
            'VX': 6,
            'B': 5,
            'N1': 27,
            'N2': 22
        }
        self.status = {
            'in': {
                'R': False,
                'BDV': False,
                'BDN': False,
            },
            'out': {
                '3': False,
                '2': False,
                '1': False,
                'VX': True,
                'B': True,
                'N1': False,
                'N2': False
            },
            'crash': {
                'N1': False,
                'N2': False
            }
        }
        self.init_gpio()

    def init_gpio(self):
        GPIO.setmode(GPIO.BCM)
        # in
        GPIO.setup(self.pins['R'], GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.pins['BDV'], GPIO.IN)
        GPIO.setup(self.pins['BDN'], GPIO.IN)
        # out
        GPIO.setup(self.pins['3'], GPIO.OUT, initial=not self.status['out']['3'])
        GPIO.setup(self.pins['2'], GPIO.OUT, initial=not self.status['out']['2'])
        GPIO.setup(self.pins['1'], GPIO.OUT, initial=not self.status['out']['1'])
        GPIO.setup(self.pins['VX'], GPIO.OUT, initial=not self.status['out']['VX'])
        GPIO.setup(self.pins['B'], GPIO.OUT, initial=not self.status['out']['B'])
        GPIO.setup(self.pins['N1'], GPIO.OUT, initial=not self.status['out']['N1'])
        GPIO.setup(self.pins['N2'], GPIO.OUT, initial=not self.status['out']['N2'])

    def get_status(self):
        return self.status

    def set_status(self, def_status):
        if def_status['in']:
            self.set_status_in(def_status['in'])
        if def_status['out']:
            self.set_status_out(def_status['out'])
        if def_status['crash']:
            self.set_status_crash(def_status['crash'])

    def set_status_in(self, def_status_in):
        self.status['in'].update(def_status_in)

    def set_status_out(self, def_status_out):
        status_out = self.status['out']
        for i in def_status_out:
            if def_status_out[i] != status_out[i]:
                GPIO.output(self.pins[i], not def_status_out[i])
        self.status['out'].update(def_status_out)

    def set_status_crash(self, def_status_crash):
        self.status['crash'].update(def_status_crash)

    def get_status_in_bdv(self):
        if GPIO.input(self.pins['BDV']) == GPIO.LOW:
            self.set_status_in({'BDV': True})
        else:
            self.set_status_in({'BDV': False})
        return self.status['in']['BDV']

    def get_status_in_bdn(self):
        if GPIO.input(self.pins['BDN']) == GPIO.LOW:
            self.set_status_in({'BDN': True})
        else:
            self.set_status_in({'BDN': False})
        return self.status['in']['BDN']

    def callback_in_r(self, channel):
        self.set_status_in({'R': True})

    def get_status_in_r(self):
        GPIO.add_event_detect(self.pins['R'], GPIO.FALLING, callback=self.callback_in_r, bouncetime=50)
        time.sleep(0.1)
        GPIO.remove_event_detect(self.pins['R'])
        return self.status['in']['R']

    def set_status_default(self):
        self.set_status_in({'R': False})
        self.set_status_crash({'N1': False, 'N2': False})
