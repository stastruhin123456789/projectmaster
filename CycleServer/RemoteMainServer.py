import json
import time
import requests


class RemoteMainServer():
    def __init__(self):
        main_server_ip = 'localhost'
        main_server_post = 5000
        self.url = 'http://{0}:{1}/'.format(main_server_ip, main_server_post)
        self.headers = {
            'content-type': 'application/json'
        }

    def get_status(self):
        method = 'status'
        try:
            data = {}
            result = requests.post(self.url + method, data=json.dumps(data), headers=self.headers)
            if result.status_code == 200:
                return result.json()
        except requests.exceptions.RequestException as e:
            print(e)
        return {}

    def set_status(self, data):
        method = 'status/edit'
        try:
            result = requests.post(self.url + method, data=json.dumps(data), headers=self.headers)
            if result.status_code == 200:
                return result.json()
        except requests.exceptions.RequestException as e:
            print(e)
        return {}

    def get_status_out_123_now(self):
        method = 'status/out/123/now'
        try:
            data = {}
            result = requests.post(self.url + method, data=json.dumps(data), headers=self.headers)
            if result.status_code == 200:
                return result.json()
        except requests.exceptions.RequestException as e:
            print(e)
        return {}

    def set_status_out_123_now(self, data):
        method = 'status/out/123/now/edit'
        try:
            result = requests.post(self.url + method, data=json.dumps(data), headers=self.headers)
            if result.status_code == 200:
                return result.json()
        except requests.exceptions.RequestException as e:
            print(e)
        return {}

    def connect_to_main_server(self):
        method = 'ping'
        while True:
            try:
                data = {}
                result = requests.post(self.url + method, data=json.dumps(data), headers=self.headers)
                if result.status_code == 200:
                    return result.json()
            except requests.exceptions.RequestException as e:
                print('.')
                time.sleep(0.5)
                continue