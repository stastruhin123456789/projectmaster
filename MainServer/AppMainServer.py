from flask import Flask, jsonify, request
from RepositoryMain import RepositoryMain

app = Flask(__name__)
repository_main = RepositoryMain()


@app.route('/ping', methods=["GET", "POST"])
def ping():
    return jsonify({'success': True}), 200, {'ContentType': 'application/json'}


@app.route('/status', methods=["POST"])
def get_status():
    return jsonify(repository_main.get_status())


@app.route('/status/edit', methods=["POST"])
def edit_status():
    repository_main.set_status(request.json)
    return jsonify({'success': True}), 200, {'ContentType': 'application/json'}


@app.route('/status/out/123/now', methods=["POST"])
def get_status_out_123_now():
    return jsonify(repository_main.get_status_out_123_now())


@app.route('/status/out/123/now/edit', methods=["POST"])
def edit_status_out_123_now():
    repository_main.set_status_out_123_now(request.json)
    return jsonify({'success': True}), 200, {'ContentType': 'application/json'}


if __name__ == '__main__':
    app.run(host='localhost', port=5000)
